import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;
import org.apache.xerces.impl.Constants;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.internal.Utils;

import static org.openqa.selenium.By.*;



public class WarranteerSanity {
    char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    private AppiumDriver driver;
    private WebDriver webDriver;
    String googleConnectID =  "Google Connect";


    private void assertKey(Map<String, String> object, String key, String expectedValue) {
        Assert.assertTrue(object.containsKey(key));
        Assert.assertEquals(expectedValue, object.get(key));
    }

    @Before
    public void setUp ()throws Exception {
        File appDir = new File("C:\\Users\\Yaniv\\Downloads");
        File app = new File(appDir, "android-release-v1.39-staging-24.apk");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "Samsung SM-G900F");
        capabilities.setCapability("platformVersion", "5.0");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("appPackage", "com.warranteer.app");
        capabilities.setCapability("appActivity", "com.warranteer.app.ui.activity.SplashScreen_");
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Thread.sleep(10000);
        driver.swipe(1000, 900, 100, 900, 1000);
        Thread.sleep(1000);
        driver.swipe(1000, 850, 100, 850,1000);
        Thread.sleep(1000);
        driver.swipe(1000, 800, 100, 800,1000);
        Thread.sleep(1000);
        driver.swipe(1000, 750, 100, 750,1000);
        Thread.sleep(1000);
        driver.swipe(1000, 700, 100, 700,1000);
        Thread.sleep(1000);
        driver.findElement(name("Continue")).click();
        Class.forName("com.mysql.jdbc.Driver");
    }




    /*
    @Test
    public void googleLoginTest ()throws Exception {
        driver.navigate().back();
        driver.findElement(name("My profile")).click();
        Helper.googleLogin(driver,"yaniv@warranteer.com","Login");
    }

    @Test
    public void googleLoginWarrantyInvoice ()throws Exception {
        Helper.addProductApplianceFromVendorWithInvoice(driver, "357246058168624");
    }

    @Test
    public void googleRegisterTest ()throws Exception {
        driver.navigate().back();
        driver.findElement(name("My profile")).click();
        Helper.googleRegister(driver ,"yaniv@warranteer.com");
        MobileElement myProfile = null;
        driver.findElement(name("My profile")).click();
        myProfile = (MobileElement) driver.findElement(name("My profile"));
        TestCase.assertNotNull(myProfile);
        String query = "select * from user order by id desc limit 1";
        List<Map<String, String>> result = SqlService.runQuery(query);
        Assert.assertEquals(1, result.size());
        assertKey(result.get(0), "email", "yaniv@warranteer.com");
        assertKey(result.get(0), "phone_number", "972526052254");
        assertKey(result.get(0), "first_name", "Yaniv");
        assertKey(result.get(0), "last_name", "Golan");
    }
*/


    @Test
    public void facebookRegisterTest ()throws Exception {
        driver.navigate().back();
        driver.findElement(name("My profile")).click();
        Helper.FacebookRegister(driver ,"'yaniv.golan9@gmail.com'");
        MobileElement myProfile = null;
        driver.findElement(name("My profile")).click();
        myProfile = (MobileElement) driver.findElement(name("My profile"));
        TestCase.assertNotNull(myProfile);
        String query = "select * from user order by id desc limit 1";
        List<Map<String, String>> result = SqlService.runQuery(query);
        Assert.assertEquals(1, result.size());
        assertKey(result.get(0), "email", "yaniv.golan9@gmail.com");
        assertKey(result.get(0), "phone_number", "972526052254");
        assertKey(result.get(0), "first_name", "Yaniv");
        assertKey(result.get(0), "last_name", "Golan");
    }





    @Test
    public void facebookLoginTest ()throws Exception {
        driver.navigate().back();
        driver.findElement(name("My profile")).click();
        Helper.facebookLogin(driver ,"Login");
    }



/*
    @Test
    public void googleRegister ()throws Exception {
        deleteuser("'yaniv@warranteer.com'");
        driver.navigate().back();
        MobileElement myProfile = null;
        driver.findElement(name("My profile")).click();
        driver.findElement(name(googleConnectID)).click();
        Thread.sleep(2000);
        driver.findElement(name("yaniv@warranteer.com")).click();
        driver.findElement(name("OK")).click();
        driver.findElement(name("OK")).click();
        MobileElement phoneNumber = (MobileElement) driver.findElement(id("com.warranteer.app:id/phoneNumberEditText"));
        MobileElement Address = (MobileElement) driver.findElement(id("com.warranteer.app:id/addressAutoComplete"));
        phoneNumber.sendKeys("526052254");
        driver.swipe(500, 1000, 500, 400, 1000);
        Address.sendKeys("Zamir 4, Lehavim, Israel");
        driver.tap(1,1000,1000,1);
        driver.findElement(id("com.warranteer.app:id/registerButton")).click();
        Thread.sleep(6000);
        myProfile = (MobileElement) driver.findElement(name("My profile"));
        TestCase.assertNotNull(myProfile);
        String query = "select * from user order by id desc limit 1";
        List<Map<String, String>> result = SqlService.runQuery(query);
        Assert.assertEquals(1, result.size());
        assertKey(result.get(0), "email", "yaniv@warranteer.com");
        assertKey(result.get(0), "phone_number", "972526052254");
        assertKey(result.get(0), "first_name", "Yaniv");
        assertKey(result.get(0), "last_name", "Golan");
        SqlService.close();
    }
    */


    /*
    @Test
    public void RegularRegister ()throws Exception {
        String newemail  = "yaniv@"+ randomString(CHARSET_AZ_09,6)+".com";
        driver.navigate().back();
        MobileElement myProfile = null;
        driver.findElement(name("My profile")).click();
        driver.findElement(id("com.warranteer.app:id/emailAccountButton")).click();
        MobileElement email = (MobileElement) driver.findElement(id("com.warranteer.app:id/emailEditText"));
        MobileElement password = (MobileElement) driver.findElement(id("com.warranteer.app:id/passwordEditText"));
        MobileElement repassword = (MobileElement) driver.findElement(id("com.warranteer.app:id/passwordAgainEditText"));
        email.sendKeys(newemail);
        password.sendKeys("123456");
        repassword.sendKeys("123456");
        driver.swipe(500, 1000, 500, 400, 1000);
        driver.findElement(id("com.warranteer.app:id/contiueButton")).click();
        driver.findElement(id("com.warranteer.app:id/firstNameEditText")).sendKeys("Yaniv");
        driver.findElement(id("com.warranteer.app:id/lastNameEditText")).sendKeys("Golan");
        driver.swipe(500, 1000, 500, 400, 1000);
        MobileElement phoneNumber = (MobileElement) driver.findElement(id("com.warranteer.app:id/phoneNumberEditText"));
        MobileElement Address = (MobileElement) driver.findElement(id("com.warranteer.app:id/addressAutoComplete"));
        phoneNumber.sendKeys("526052254");
        Address.sendKeys("Zamir 4, Lehavim, Israel");
        driver.swipe(500, 1000, 500, 400, 1000);
        driver.tap(1,1000,1000,1);
        driver.findElement(id("com.warranteer.app:id/registerButton")).click();
        Thread.sleep(6000);
        myProfile = (MobileElement) driver.findElement(name("My profile"));
        TestCase.assertNotNull(myProfile);
        String query = "select * from user order by id desc limit 1";
        List<Map<String, String>> result = SqlService.runQuery(query);
        Assert.assertEquals(1, result.size());
        assertKey(result.get(0), "email", newemail);
        assertKey(result.get(0), "phone_number", "972526052254");
        assertKey(result.get(0), "first_name", "Yaniv");
        assertKey(result.get(0), "last_name", "Golan");
    }
    */


/*
    @Test
    public void addNewProductFacebookRegister ()throws Exception {
        addProductWithInvoice("1234");
        driver.findElement(id("android:id/button1")).click();
        FacebookRegister("'yaniv.golan9@gmail.com'");
        SqlService.close();



    }
*/

    /*
    @Test
    public void Mobileyeflow() throws SQLException {
        System.out.print(mobileye);
        executeCommand(mobileye);
        assert(true);
    }
    */




    @After
    public void tearDown ()throws Exception {
        driver.quit();
    }

}




