import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Yaniv on 18/05/2015.
 */
public class SqlService {

    public static final String  delete_user_facebook1 =  "DROP TABLE IF EXISTS appliances_to_delete ; " ;
    public static final String  delete_user_facebook2 =  "DROP TABLE IF EXISTS products_to_delete ; ";
   /* public static final String  delete_user_facebook3 =  "SELECT @user_id := id  FROM user WHERE email = 'yaniv.golan9@gmail.com' ; "; */
   public static final String  delete_user_facebook3 =  "SELECT @user_id := id  FROM user WHERE email = ";
    public static final String  delete_user_facebook4 =  "CREATE TABLE IF NOT EXISTS appliances_to_delete (appliance_id LONG, product_id LONG) ; "  ;
    public static final String  delete_user_facebook5 =  "INSERT INTO appliances_to_delete (appliance_id, product_id) SELECT a.id, a.product_id FROM appliance a JOIN warranty w ON w.appliance_id=a.id WHERE w.owner_user_id=@user_id AND a.source='UserAdded' ;";
    public static final String  delete_user_facebook6 =  "CREATE TABLE IF NOT EXISTS products_to_delete (product_id LONG); ";
    public static final String  delete_user_facebook7 =  "INSERT INTO products_to_delete (product_id)SELECT p.id FROM product p JOIN appliance a ON a.product_id=p.id JOIN appliances_to_delete d ON d.product_id=p.id WHERE p.source='UserAdded' GROUP BY p.id HAVING COUNT(*)=1; ";
    public static final String  delete_user_facebook8 =  "DELETE FROM warranty WHERE owner_user_id=@user_id;";
    public static final String  delete_user_facebook9 =  "DELETE FROM appliance WHERE id IN ( SELECT appliance_id FROM appliances_to_delete);";
    public static final String  delete_user_facebook10 = "DELETE FROM product WHERE id IN (SELECT product_id FROM products_to_delete);";
    public static final String  delete_user_facebook11 = "DELETE FROM `user` WHERE id=@user_id;";
    public static final String  delete_user_facebook12 = "DROP TABLE IF EXISTS appliances_to_delete;";
    public static final String  delete_user_facebook13 = "DROP TABLE IF EXISTS products_to_delete;";

    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://173.194.225.246:3306/warranteer?user=root";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "asusPaperless4382";

    private static Connection _connection;

    private static Connection getConnection() throws SQLException {
        if (_connection != null) {
            return _connection;
        }
        _connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        return _connection;
    }

    public static List<Map<String, String>> runQuery(String query) throws SQLException {
        Connection dbConnection = getConnection();
        Statement statement = null;
        try {
            statement = dbConnection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            return mapResultSet(rs);
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }


    public static void UpdateQuery(String query) throws SQLException {
        Connection dbConnection = getConnection();
        Statement statement = null;
        try {
            statement = dbConnection.createStatement();
            statement.executeUpdate(query);

        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }


    public static void execQuery(String query) throws SQLException {
        Connection dbConnection = getConnection();
        Statement statement = null;
        try {
            statement = dbConnection.createStatement();
            statement.execute(query);

        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }


    private static List<Map<String, String>> mapResultSet(ResultSet rs) throws SQLException {
        List<Map<String, String>> resultRows = new ArrayList<Map<String, String>>();
        while (rs.next()) {
            Map<String, String> row = new HashMap();
            resultRows.add(row);
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                row.put(rsmd.getColumnName(i), rs.getString(i));
            }
        }
        return resultRows;
    }

    public static void close() {
        if (_connection != null) {
            try {
                _connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
