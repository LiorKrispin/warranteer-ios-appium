/**
 * Created by Yaniv on 19/07/2015.
 */
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.Random;

import junit.framework.TestCase;
import org.apache.xerces.impl.Constants;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.internal.Utils;

import static org.openqa.selenium.By.*;


public class Helper {

    public static void takePicture(AppiumDriver driver, String device) throws InterruptedException {
        Thread.sleep(2000);
        driver.tap(1,550,1800,2);
        Thread.sleep(2000);
        driver.findElement(name("OK")).click();
        Thread.sleep(2000);

    }


    public static String randomString(char[] characterSet, int length) {
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }



    public static void addProductApplianceFromVendorWithInvoice(AppiumDriver driver,String imei) throws InterruptedException,Exception{
        driver.findElement(id("com.warranteer.app:id/addOtherDeviceWarrantyWithBoxButton")).click();
        driver.findElement(id("com.warranteer.app:id/flow_scan_skip")).click();
        MobileElement barcode = (MobileElement) driver.findElement(id("com.warranteer.app:id/flow_product_barcode_value"));
        barcode.sendKeys(imei);
        driver.findElement(id("com.warranteer.app:id/flow_product_brand_value")).click();
        driver.findElement(name("OK")).click();
        driver.findElement(id("com.warranteer.app:id/next_button")).click();
        driver.findElement(id("com.warranteer.app:id/use_camera_button")).click();
        takePicture(driver,"samsung-s5");
        driver.findElement(id("com.warranteer.app:id/flow_warranty_purchase_place_value")).sendKeys("bug");
        driver.swipe(500, 1000, 500, 400, 1000);
        driver.findElement(id("com.warranteer.app:id/flow_warranty_price_value")).sendKeys("1000");
        driver.swipe(500, 1000, 500, 400, 1000);
        driver.findElement(id("com.warranteer.app:id/done_button")).click();
        driver.findElement(id("android:id/button1")).click();
        googleLogin(driver, "yaniv@warranteer.com","Product");



    }

    public static void googleLogin (AppiumDriver driver,String email,String flow)throws Exception {
        MobileElement myProfile = null;
        WebElement extraOK = null;
        driver.findElement(name("Google Connect")).click();
        driver.findElement(name(email)).click();
        driver.findElement(name("OK")).click();
        if(!driver.findElements(By.name("OK")).isEmpty()) {
            extraOK =  driver.findElement(name("OK"));
            extraOK.click();
        }
        if(flow.equals("Login")) {
            myProfile = (MobileElement) driver.findElement(name("My profile"));
            TestCase.assertNotNull(myProfile);
        }
        if(flow.equals("Product")) {
            Thread.sleep(2000);
            driver.findElement(name("Skip")).click();


        }
    }

    public static void googleRegister(AppiumDriver driver , String email) throws InterruptedException,SQLException {
        deleteuser("'"+email+"'");
        WebElement extraOK = null;
        driver.findElement(name("Google Connect")).click();
        driver.findElement(name(email)).click();
        driver.findElement(name("OK")).click();
        if(!driver.findElements(By.name("OK")).isEmpty()) {
            extraOK =  driver.findElement(name("OK"));
            extraOK.click();
        }
        Thread.sleep(3000);
        MobileElement phoneNumber = (MobileElement) driver.findElement(id("com.warranteer.app:id/phoneNumberEditText"));
        MobileElement Address = (MobileElement) driver.findElement(id("com.warranteer.app:id/addressAutoComplete"));
        phoneNumber.sendKeys("526052254");
        driver.swipe(500, 1000, 500, 400, 1000);
        Address.sendKeys("Zamir 4, Lehavim, Israel");
        driver.tap(1, 1000, 1000, 1);
        driver.findElement(id("com.warranteer.app:id/registerButton")).click();
        Thread.sleep(6000);
    }




    public void addProductWithInvoice(AndroidDriver driver, String imei) throws InterruptedException{
        driver.navigate().back();
        driver.findElement(name("Add a new warranty")).click();
        driver.findElement(name("Scan product’s barcode")).click();
        driver.findElement(name("or enter manually")).click();
        MobileElement fieldOne = (MobileElement) driver.findElement(name("barcode"));
        fieldOne.sendKeys(imei);
        Thread.sleep(2000);
        driver.findElement(name("OK")).click();
        Thread.sleep(2000);
        driver.findElement(name("OK")).click();
        Thread.sleep(2000);
        driver.findElement(name("Continue")).click();
        Thread.sleep(2000);
        driver.findElement(id("com.warranteer.app:id/cameraScanImageButton")).click();
        driver.findElement(id("android:id/text1")).click();
        Thread.sleep(2000);
        driver.tap(1,550,1800,2);
        Thread.sleep(1000);
        driver.findElement(name("OK")).click();
        driver.findElement(id("android:id/button1")).click();
        driver.findElement(name("Add product")).click();
        Thread.sleep(6000);
    }


    public static void deleteuser(String email) throws SQLException {
        email= email+   ";";
        SqlService.UpdateQuery(SqlService.delete_user_facebook1);
        SqlService.UpdateQuery(SqlService.delete_user_facebook2);
        SqlService.runQuery(SqlService.delete_user_facebook3+email);
        SqlService.UpdateQuery(SqlService.delete_user_facebook4);
        SqlService.UpdateQuery(SqlService.delete_user_facebook5);
        SqlService.UpdateQuery(SqlService.delete_user_facebook6);
        SqlService.UpdateQuery(SqlService.delete_user_facebook7);
        SqlService.UpdateQuery(SqlService.delete_user_facebook8);
        SqlService.UpdateQuery(SqlService.delete_user_facebook9);
        SqlService.UpdateQuery(SqlService.delete_user_facebook10);
        SqlService.UpdateQuery(SqlService.delete_user_facebook11);
        SqlService.UpdateQuery(SqlService.delete_user_facebook12);
        SqlService.UpdateQuery(SqlService.delete_user_facebook13);
    }



    public static void FacebookRegister(AppiumDriver driver , String email) throws InterruptedException,SQLException {
        deleteuser(email);
        driver.findElement(id("com.warranteer.app:id/facebookLoginButton")).click();
        Thread.sleep(3000);
        MobileElement phoneNumber = (MobileElement) driver.findElement(id("com.warranteer.app:id/phoneNumberEditText"));
        MobileElement Address = (MobileElement) driver.findElement(id("com.warranteer.app:id/addressAutoComplete"));
        phoneNumber.sendKeys("526052254");
        driver.swipe(500, 1000, 500, 400, 1000);
        Address.sendKeys("Zamir 4, Lehavim, Israel");
        driver.tap(1, 1000, 1000, 1);
        driver.findElement(id("com.warranteer.app:id/registerButton")).click();
        Thread.sleep(6000);
    }





    public static void facebookLogin (AppiumDriver driver ,String flow)throws Exception {
        MobileElement myProfile = null;

        driver.findElement(id("com.warranteer.app:id/facebookLoginButton")).click();
        Thread.sleep(1000);
        if(flow.equals("Login")) {
            myProfile = (MobileElement) driver.findElement(name("My profile"));
            TestCase.assertNotNull(myProfile);
        }
        if(flow.equals("Product")) {
            Thread.sleep(2000);
            driver.findElement(name("Skip")).click();


        }
    }







}



















































/* --------------------------------------------------    end    ---------------------------------------------------  *\
 /*


    public void addProductWithoutInvoiceOLD(AndroidDriver driver) throws InterruptedException{
        driver.navigate().back();
        driver.findElement(name("Add a new warranty")).click();
        driver.findElement(name("Scan product’s barcode")).click();
        driver.findElement(name("or enter manually")).click();
        MobileElement fieldOne = (MobileElement) driver.findElement(name("barcode"));
        fieldOne.sendKeys("1234");
        Thread.sleep(2000);
        driver.findElement(name("OK")).click();
        Thread.sleep(2000);
        driver.findElement(name("OK")).click();
        Thread.sleep(2000);
        driver.findElement(name("Continue")).click();
        Thread.sleep(2000);
        driver.findElement(name("Skip")).click();
        Thread.sleep(4000);
        driver.findElement(name("Add product")).click();
        Thread.sleep(6000);
    }
*/


/*

    private String executeCommand(String command) {
        StringBuffer output = new StringBuffer();
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
            // wait for 1 second and then destroy the process
            Thread.sleep(1000);
            p.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }
*/

    /*
    @Test
    public void testSessions() throws Exception {
        HttpGet request = new HttpGet("https://warranteer-qa.appspot.com/%@facebookLogin?uuid=%@&token=%@&email=%@&lang=%@&remember=true");
        @SuppressWarnings("resource")
        HttpClient httpClient = new DefaultHttpClient();
        org.apache.http.HttpResponse response = httpClient.execute(request);
        System.out.print (response);
        HttpEntity entity = response.getEntity();
        System.out.print (entity);
        JSONObject jsonObject = (JSONObject) new JSONParser().parse(EntityUtils.toString(entity));
        System.out.print (jsonObject);
        String sessionId = driver.getSessionId().toString();
        Assert.assertEquals(jsonObject.get("sessionId"), sessionId);
    }  */




    /*
    @Test
    public void addNewProductFacebookLogin ()throws Exception {
        driver.findElement(id("com.warranteer.app:id/addOtherDeviceWarrantyWithBoxButton")).click();
        driver.findElement(id("com.warranteer.app:id/manualBarcodeButton")).click();
        MobileElement fieldOne = (MobileElement) driversel.findElement(name("barcode"));
        fieldOne.sendKeys("1234");
        driver.findElement(name("Save")).click();
        Thread.sleep(6000);
        driver.findElement(name("Save")).click();
        Thread.sleep(4000);
        driver.findElement(name("Continue")).click();
        driver.findElement(name("Skip")).click();
        driver.findElement(name("Add product")).click();
               try {
                   waitForElement(By.partialLinkText("Your Toast message"), 4, driversel);
            System.out.print("Found!!!");

        }
        catch (Exception ignore)
        {System.out.print("not-found");
        }
        Thread.sleep(50000);

    } */

      /*
        Thread.sleep(10000);
        driver.swipe(1000, 900, 100, 900, 1000);
        Thread.sleep(2000);
        driver.swipe(1000, 850, 100, 850,1000);
        Thread.sleep(2000);
        driver.swipe(1000, 800, 100, 800,1000);
        Thread.sleep(2000);
        driver.swipe(1000, 750, 100, 750,1000);
        Thread.sleep(2000);
        driver.swipe(1000, 700, 100, 700,1000);
        Thread.sleep(2000);
        driver.findElement(name("Continue")).click();
        driver.findElement(name("Scan product’s barcode")).click();
        driver.findElement(name("or enter manually")).click();
        MobileElement fieldOne = (MobileElement) driver.findElement(name("barcode"));
        fieldOne.sendKeys("1234");
        Thread.sleep(4000);
        driver.findElement(name("Save")).click();
        Thread.sleep(6000);
        driver.findElement(name("Save")).click();
        Thread.sleep(4000);
        driver.findElement(name("Continue")).click();
        Thread.sleep(4000);
        driver.findElement(name("Skip")).click();
        Thread.sleep(4000);
        driver.findElement(name("Add product")).click();
        Thread.sleep(6000);   */





    /*    // we are on second screen now
        // check if second screen contains TextView with text “Activity2”
        driver.findElement(By.id("Scan product's barcode"));

        // click back button
        HashMap<String, Integer> keycode = new HashMap<String, Integer>();
        keycode.put("keycode", 4);
        ((JavascriptExecutor) driver).executeScript("mobile: keyevent", keycode);

        // we are again in main activity
        driver.findElement(By.name("Button1"));  */




   /*
    @Test
    public void updateprofile ()throws Exception {
        // find button with label or content-description "Button 1"
        driver.findElement(name("My profile")).click();
        driver.findElement(id("com.warranteer.app:id/firstNameEditText")).clear();
        driver.findElement(id("com.warranteer.app:id/updateProfileButton")).click();

        Thread.sleep(1000);
    }
    */




  /*
    @Test
    public void testSessions() throws Exception {
        HttpGet request = new HttpGet("http://localhost:4723/wd/hub/sessions");
        @SuppressWarnings("resource")
        HttpClient httpClient = new HttpClient();
        HttpResponse response = httpClient.execute(request);
        HttpEntity entity = getAttribute(response);
        JSONObject jsonObject = (JSONObject) new JSONParser().parse(EntityUtils.toString(entity));

        String sessionId = driver.getSessionId().toString();
        Assert.assertEquals(jsonObject.get("sessionId"), sessionId);
    }

    */

   /*
    @Test
    public void updateprofile ()throws Exception {
        // find button with label or content-description "Button 1"
        driver.findElement(name("My profile")).click();
        driver.findElement(id("com.warranteer.app:id/firstNameEditText")).clear();
        driver.findElement(id("com.warranteer.app:id/firstNameEditText")).sendKeys("baba");

        driver.findElement(id("com.warranteer.app:id/updateProfileButton")).click();

        Thread.sleep(1000);


    }
*/














/*
    public static void takeScreenshot(final Driver driver, final String fileName)
            throws TimeoutException, AdbCommandRejectedException, IOException {
        final ImageData imageData = driver.getScreenshot();
        final ImageLoader loader = new ImageLoader();
        loader.data = new ImageData[] { imageData };
        loader.save(new File(fileName).getAbsolutePath */